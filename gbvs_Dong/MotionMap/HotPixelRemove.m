function im_a = HotPixelRemove( im )
nbins = 1000;
[n, x] = hist(im(:),nbins);
CumHist = zeros(size(n));
CumHist(1) = n(1);
for i = 2 : 1: nbins
    CumHist(i) = CumHist(i-1) + n(i);
end

CumHist = CumHist./(size(im, 1)*size(im, 2));
index = find(CumHist>=0.997);

in = find(im>=x(index(1)+1));
im_a = im;
im_a(in)= x(index(1)+1);

end

