function map = MotionMap(  img , img_prev, MapMaxSize )
if ( (nargin < 3) )
    MapSize = size(img)/16;
else
    MapSize = round([min(size(img))* MapMaxSize/max(size(img)) MapMaxSize]);
end

H = fspecial('gaussian', [10 ,10], 0.5);
blurred = imfilter(img_prev,H,'replicate');
img_prev_r = img_prev - blurred;

blurred = imfilter(img,H,'replicate');
img_r = img - blurred;

opticalFlow = vision.OpticalFlow ;
opticalFlow. ReferenceFrameSource = 'Input port';
% opticalFlow. Method = 'Lucas-Kanade';
 v = step(opticalFlow,img_r,img_prev_r);

 v = HotPixelRemove( v );
%v = medfilt2(v);
%v = mat2gray(medfilt2(v));

level = graythresh(v);
v = imresize(v,  MapSize,'bilinear');  % downsmaple to denoise

map = medfilt2(v);
map=mat2gray(map);
end

