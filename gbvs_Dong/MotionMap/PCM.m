


clear all
close all
resultFolder ='Z:\Temp\CaraHDR Data Feb14\HDR frames\playground\';

directory_name = uigetdir;
fileFolder=directory_name;
dirOutput = dir(fullfile(fileFolder,'*.hdr'));
fileNames = {dirOutput.name}';
N = numel(fileNames);
gap = 1;
window = 10;
delta = 0.0001;

for i = 1 : N
    fname{i} = strcat(fileFolder,'\',fileNames{i});
end
count = 0;
GroupOfImg = {};
% for i = 1:N
%     img = makeLuminanceMap(hdrread(fname{i}));
%     if (i >gap)
%         
%     end 
% end
for i = gap + 1 : N
    img = makeLuminanceMap(hdrread(fname{i}));
    img_b = makeLuminanceMap( hdrread(fname{i-gap}));
    img_a = makeLuminanceMap(hdrread(fname{i+gap}));
    numPixels = size(img,1) * size(img,2);
    key = exp((1/numPixels)*(sum(sum(log(img + delta)))));
    if (i <= (gap +window))
        temp = abs(img - img_b) + abs (img_a-img);
        temp(find(temp <key))=0;
        temp(find(temp >=key))=1;
        PCMFrame{i-gap} = temp;
        
    else
        PCMSum = zeros(size(img));
        for t =  1 : window
            PCMSum = PCMSum + PCMFrame{t};
        end
        for j = 1 :window-1;
            PCMFrame{j}=PCMFrame{j+1};
        end
        temp = abs(img - img_b) + abs (img_a-img);
        temp(find(temp <key))=0;
        temp(find(temp >=key))=1;
        PCMFrame(window) = temp;
        
    end
%     level = graythresh(PCMFrame{i});
%     index = find(PCMFrame{i}<level);
%     PCMFrame{i}(index) = 0;
end
sum = zeros(size(img));
for j = gap + 1 : 11
    sum = sum + PCMFrame{j};
end