
clear all
close all
resultFolder ='Z:\Temp\CaraHDR Data Feb14\HDR frames\fishing\Motion\temp\';

directory_name = uigetdir;
fileFolder=directory_name;
dirOutput = dir(fullfile(fileFolder,'*.hdr'));
fileNames = {dirOutput.name}';
N = numel(fileNames);

for i = 1 : N
    fname{i} = strcat(fileFolder,'\',fileNames{i});
end
params.frameGap = 5;
params.salmapmaxsize = 50;
T = params.frameGap;
load('TVI_Daly')
motion_buffer = {};     % size is equal to T

for i = 1 : T
    img = hdrread(fname{i});
    img_Luminance = makeLuminanceMap(img);
    img_luma = interp1(Luminance,Luma, img_Luminance);
    motion_buffer{i} = img_luma;
end

for i = T+1 : N
    img = hdrread(fname{i});
    img_Luminance = makeLuminanceMap(img);

    img_luma = interp1(Luminance,Luma, img_Luminance);
    Motion{i} = MotionMap(img_luma, motion_buffer{1});
    
    for j = 1 : T-1
        motion_buffer{j} = motion_buffer{j+1};
    end
    motion_buffer{T} = img_luma;
    imwrite(Motion{i}, strcat(resultFolder,num2str(i),'.png'), 'png')
end
