% color chromatic adaptation
% using CIECAM02, CAT02
% input hdr is image in cd/m2, RGBE 
% output is adapted XYZ image

function XYZ_adapt = CAT( hdr )

hdr = double(hdr); % in case input is single
size_img = size(hdr);
XYZimg = RGB2XYZ(hdr); % RGBE 2 XYZ

% First things first...define the XYZ to RGB transform under CAT02 space
M_cat02 = [ [0.7328,  0.4296, -0.1624];...
      [-0.7036, 1.6974,  0.0061];...
      [ 0.0030, 0.0136,  0.9834] ]';

  for i = 1 : 3;
       RGB_img(:,:,i) =  M_cat02(i,1)*XYZimg (:,:,1) + M_cat02(i,2)*XYZimg (:,:,2) + M_cat02(i,3)* XYZimg(:,:,3);
  end
  
% luminanc
La = 0.265*hdr(:,:,1) + 0.670*hdr(:,:,2) + 0.065*hdr(:,:,3);  

% La= 0:5:max(max(L));
% k= 1./(5*La+1);
% Fl=0.2.*(k.^4).*(5*La)+0.1.*((1-k.^4).^2).*((5*La).^(1/3));
% plot(La,Fl);

F =1;
D = F.* (1-(1/3.6)*exp(-1*(La+42)/92));


% Tristimulus values of white point
% Table A4 Page 294 Ohta Robinson
% Illuminant  X        Y       Z
%    A       109.85   100.00  35.58
%    D65      95.04   100.00 108.89
%    C        98.07   100.00 118.23
%    D50      96.42   100.00  82.49
%    D55      95.68   100.00  92.14
%    D75      94.96   100.00 122.61
%    B        99.09   100.00  85.31

Xw=95.04;
Yw=100.00;
Zw=108.89;

Rw= M_cat02(1,:)*[Xw, Yw, Zw].';
Gw= M_cat02(2,:)*[Xw, Yw, Zw].';
Bw= M_cat02(3,:)*[Xw, Yw, Zw].';

Rc=[(Yw*D/Rw)+(1-D)].*RGB_img(:,:,1);
Gc=[(Yw*D/Gw)+(1-D)].*RGB_img(:,:,2);
Bc=[(Yw*D/Bw)+(1-D)].*RGB_img(:,:,3);

RGB_adapt (:,:,1) = Rc;
RGB_adapt (:,:,2) = Gc;
RGB_adapt (:,:,3) = Bc;

%%%%%%%%%%%%%%%%%%%%%%% use CAT 02 space take RGB back to XYZ
Mi_cat02 = inv(M_cat02);

  for i = 1 : 3;
       XYZ_adapt(:,:,i) =  Mi_cat02(i,1)*RGB_adapt (:,:,1) + Mi_cat02(i,2)*RGB_adapt (:,:,2) + Mi_cat02(i,3)* RGB_adapt(:,:,3);
  end

end

