%%%% daly CSF, 1D, to generate graph

function [p, CSF] = CSF_Daly( La )
%  p: spatial frequency, cycles/deg

p = 0: 0.1:60;
P = 250;
theta = 0;
d = 1.5;

ra = 0.856 * d ^0.14;
rc = 1;
rtheta = 0.11*cos(4*theta)+0.89;

Al =0.801 * (1+0.7./La)^(-0.2);
Bl = 0.3*(1+100./La)^0.15;
S1_p = ((3.23 * p.^(-0.6)).^5 +1).^(-0.2) .* Al.*0.9.*p.*exp(- (Bl.*0.9.*p)).*(1 + 0.06*exp(Bl.*0.9.*p)).^0.5;
S1_pr = ((3.23 * (p/(ra*rc*rtheta)).^(-0.6)).^5 +1).^(-0.2) .* Al.*0.9.*p.*exp(- (Bl.*0.9.*(p/(ra*rc*rtheta)))).*(1 + 0.06*exp(Bl.*0.9.*(p/(ra*rc*rtheta)))).^0.5;

S = min(S1_pr,S1_p);
CSF = P*S;
end

