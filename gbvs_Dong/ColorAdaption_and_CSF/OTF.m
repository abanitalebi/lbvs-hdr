% input im is intensity of hdr image, A*B

function im_OTF = OTF( im )


Lga = GlobalAveLuminance(im);
d = 4.9 - 3* tanh(0.4*(log10(Lga+1)));
index = logspace( -2, 2, 400 ); % cycles/degree, ragne, 0.01 to 100
index = [0, index];   % add 0 to the scale
[I,J] = meshgrid(index);  % I and J is in cycles/degree
D = ( I.^2 + J.^2 ).^0.5;  % distance to origin
OTF = exp(- (D./(20.9 - 2.1*d)).^(1.3-0.07*d)); 

resolution = [size(im,2) size(im,1)];  % rows = 768, columns = 1024
[ppd_h, ppd_v] = pix_per_deg (40,resolution,150);  


u = resolution(1)* I ./ ppd_h ; 
v = resolution(2)* J ./ ppd_v ; 

[XI, YI] = meshgrid (0:resolution(1)/2, 0:resolution(2)/2);
ZI = interp2(u, v, OTF, XI, YI,'linear',0);

OTF2 = zeros (resolution)';
OTF2(resolution(2)/2 : resolution(2), resolution(1)/2 : resolution(1)) = ZI;

size_ZI = size(ZI);
ZI_crop = ZI (2:size_ZI(1)-1, 2:size_ZI(2)-1); 

OTF2 (resolution(2)/2 : resolution(2), 1 : (resolution(1)/2-1)) = fliplr(ZI(:,2:size_ZI(2)-1));
OTF2 (1:resolution(2)/2-1, resolution(1)/2 : resolution(1)) = flipud(ZI(2:size_ZI(1)-1,:));
OTF2(1:resolution(2)/2-1, 1:resolution(1)/2-1 ) = flipud(fliplr(ZI_crop));

IM = fft2 (im);
IM_OTF = fftshift(IM).*OTF2;
im_OTF = ifft2 (ifftshift(IM_OTF));
im_OTF = abs(im_OTF);
end

