function result = RGB2XYZ( im )

RGBtoXYZ  = [0.5141364, 0.3238786,  0.16036376;...
			0.265068,  0.67023428, 0.06409157;...
			0.0241188, 0.1228178,  0.84442666];

        for i = 1 : 3;
                result(:,:,i) =  RGBtoXYZ(i,1)*im (:,:,1) + RGBtoXYZ(i,2)*im (:,:,2)+RGBtoXYZ(i,3)*im (:,:,3);
        end
        
end

