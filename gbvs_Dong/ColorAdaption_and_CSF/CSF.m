% LA   addaption luminance  LA = 0.0001, 0.01,1,100,1000
% im is the intensity of an image
% X= 10; % angular object area , refer to Barten
% this function calculate 2D CSF function and apply the function to grey image im 
function im_CSF = CSF(im, LA,X)


size_im = size(im);
resolution = [size_im(2) size_im(1)];
[ppd_h, ppd_v] = pix_per_deg (40,resolution,150);  

%%%%%%%%%%%%%% 3D CSF
index = logspace( -2, 2, 256 ); % cycles/degree, ragne, 0.01 to 100
index = [0, index];   % add 0 to the scale
[I,J] = meshgrid(index);  % I and J is in cycles/degree
 
u = resolution(1)* I ./ ppd_h ; 
v = resolution(2)* J ./ ppd_v ; 
% u, v is in cycles /image
% meanningful range for u,v: 0 to rows/2 in vertical, 0 to columns/2 in hotizontal
 
D = ( I.^2 + J.^2 ).^0.5;  % distance to origin
 
MOPT = exp (-0.0016 * (D.^2) *(1+100/LA).^0.08); 
A = 3700 * MOPT; 
B = ((1+ 144./(X.^2) + 0.64* D.^2).*(64./(LA.^0.83) + 1./(1-exp(-0.02*(D.^2))))).^0.5 ;
S = A./B ;
SN= S/ max(max(S));   % normalized senstivity
% mesh (u,v, SN); hold all

[XI, YI] = meshgrid (0:resolution(1)/2, 0:resolution(2)/2);
ZI = interp2 (u,v,SN,XI,YI,'linear',0);


CSF = zeros (resolution)';
CSF(resolution(2)/2 : resolution(2), resolution(1)/2 : resolution(1)) = ZI;

size_ZI = size(ZI);
ZI_crop = ZI (2:size_ZI(1)-1, 2:size_ZI(2)-1); 

CSF (resolution(2)/2 : resolution(2), 1 : (resolution(1)/2-1)) = fliplr(ZI(:,2:size_ZI(2)-1));
CSF (1:resolution(2)/2-1, resolution(1)/2 : resolution(1)) = flipud(ZI(2:size_ZI(1)-1,:));
CSF(1:resolution(2)/2-1, 1:resolution(1)/2-1 ) = flipud(fliplr(ZI_crop));
%%%%%%%%%%%%%%%%%%%%%%% fft and apply CSF

IM = fft2 (im);
IM_CSF = fftshift(IM).*CSF;
im_CSF = ifft2 (ifftshift(IM_CSF));
im_CSF = abs(im_CSF);
% im_CSF=mat2gray(im_CSF);
% im_CSF = im_CSF.^ (0.5);

end
