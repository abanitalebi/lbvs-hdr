% multiscale CSF, with default maximum luminance, 20000
% input im should be luminance of HDR image
% Luminance adaption values LA could be changed for images with different
% dynamic range

function result = msCSF3( i, Luminance )
    max_L = 20000; 
    Luminance = Luminance./max(i(:)) * max_L;
    Luminance(find(Luminance<0.00000001)) = 0.00000001;
    
LA = [0.0001, 0.01, 0.1, 1 , 10, 100, 1000]; % Luminance adaption

for k = 1: length(LA)
    im_CSF{k} = CSF(Luminance, LA(k),10);  %% im_CSF may contain complex number
end

size_im = size(i);
for m = 1 : size_im(1)
    for n = 1 : size_im(2)
        
      if Luminance (m,n) < LA(1)
            im_CSF_combined (m,n) = im_CSF{1}(m,n);
      elseif Luminance(m,n) >= LA(1) & Luminance(m,n) < LA(2)
            im_CSF_combined (m,n) = interp1 ([LA(1),LA(2)], [im_CSF{1}(m,n),im_CSF{2}(m,n)], Luminance(m,n));
      elseif  Luminance(m,n) >= LA(2) & Luminance(m,n) < LA(3)
            im_CSF_combined (m,n) = interp1 ([LA(2),LA(3)], [im_CSF{2}(m,n),im_CSF{3}(m,n)], Luminance(m,n));
      elseif Luminance(m,n) >= LA(3) & Luminance(m,n) < LA(4)
            im_CSF_combined (m,n) = interp1 ([LA(3),LA(4)], [im_CSF{3}(m,n),im_CSF{4}(m,n)], Luminance(m,n));
      elseif  Luminance(m,n) >= LA(4) & Luminance(m,n) < LA(5)
            im_CSF_combined (m,n) = interp1 ([LA(4),LA(5)], [im_CSF{4}(m,n),im_CSF{5}(m,n)], Luminance(m,n));
      elseif Luminance(m,n) >= LA(5) & Luminance(m,n) < LA(6)
            im_CSF_combined (m,n) = interp1 ([LA(5),LA(6)], [im_CSF{5}(m,n),im_CSF{6}(m,n)], Luminance(m,n));
      elseif Luminance(m,n) >= LA(6) & Luminance(m,n) < LA(7)
            im_CSF_combined (m,n) = interp1 ([LA(6),LA(7)], [im_CSF{6}(m,n),im_CSF{7}(m,n)], Luminance(m,n));
      else 
            im_CSF_combined (m,n) = im_CSF{7}(m,n);
      end
                        
        
    end
end

result = abs(im_CSF_combined);
result = result./max(result(:));

end

