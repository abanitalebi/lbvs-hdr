
% i should be luminance of HDR image
function result = msCSF_Daly(i)

LA = [0.0001, 0.01, 0.1, 1 , 10, 100, 1000]; % Luminance adaption

for k = 1: length(LA)
    im_CSF{k} = imCSF_Daly(i, LA(k));
end

size_im = size(i);
for m = 1 : size_im(1)
    for n = 1 : size_im(2)
        
      if i (m,n) < LA(1)
            im_CSF_combined (m,n) = im_CSF{1}(m,n);
      elseif i(m,n) >= LA(1) & i(m,n) < LA(2)
            im_CSF_combined (m,n) = interp1 ([LA(1),LA(2)], [im_CSF{1}(m,n),im_CSF{2}(m,n)], i(m,n));
      elseif  i(m,n) >= LA(2) & i(m,n) < LA(3)
            im_CSF_combined (m,n) = interp1 ([LA(2),LA(3)], [im_CSF{2}(m,n),im_CSF{3}(m,n)], i(m,n));
      elseif i(m,n) >= LA(3) & i(m,n) < LA(4)
            im_CSF_combined (m,n) = interp1 ([LA(3),LA(4)], [im_CSF{3}(m,n),im_CSF{4}(m,n)], i(m,n));
      elseif  i(m,n) >= LA(4) & i(m,n) < LA(5)
            im_CSF_combined (m,n) = interp1 ([LA(4),LA(5)], [im_CSF{4}(m,n),im_CSF{5}(m,n)], i(m,n));
      elseif i(m,n) >= LA(5) & i(m,n) < LA(6)
            im_CSF_combined (m,n) = interp1 ([LA(5),LA(6)], [im_CSF{5}(m,n),im_CSF{6}(m,n)], i(m,n));
      elseif i(m,n) >= LA(6) & i(m,n) < LA(7)
            im_CSF_combined (m,n) = interp1 ([LA(6),LA(7)], [im_CSF{6}(m,n),im_CSF{7}(m,n)], i(m,n));
      else 
            im_CSF_combined (m,n) = im_CSF{7}(m,n);
      end
                        
        
    end
end

result = abs(im_CSF_combined);

end

