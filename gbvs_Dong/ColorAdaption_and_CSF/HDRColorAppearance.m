% input hdr image in RGBE format
% using extended color model by m.h. Kim

function [ RG, BY, A ] = HDRColorAppearance( hdr )

XYZ_adapt = CAT(hdr);
LMS = XYZ2LMS(XYZ_adapt);

% cone response 

nc = 0.57;
La = 0.265*hdr(:,:,1) + 0.670*hdr(:,:,2) + 0.065*hdr(:,:,3);

L = abs(LMS(:,:,1).^nc ./(LMS(:,:,1).^nc + La.^nc+0.00001)); % when negative value, nc less than 1, there are compex numbers in the result
M = abs(LMS(:,:,2).^nc ./(LMS(:,:,2).^nc + La.^nc+0.00001));
S = abs(LMS(:,:,3).^nc ./(LMS(:,:,3).^nc + La.^nc+0.00001));

RG= (11*L -12*M + S)/11 ; 
BY= (L + M - 2*S)/9;
RG = medfilt2(RG);
BY = medfilt2(BY);
% RG = abs( RG.^0.75 );
% BY = abs( BY.^0.75);

A = (40*L+20*M+S)/61;

end

