% LA   addaption luminance  LA could be 0.0001, 0.01,1,100,1000
% im is the intensity of an image
% function include OTF

function im_CSF = imCSF_Daly( im, La )
size_im = size(im);
resolution = [size_im(2) size_im(1)];
[ppd_h, ppd_v] = pix_per_deg (40,resolution,150);

%%%%%%%%%%%%%%  CSF
index = logspace( -2, 2, 256 ); % cycles/degree, ragne, 0.01 to 100
index = [0, index];   % add 0 to the scale
[I,J] = meshgrid(index);  % I and J is in cycles/degree
 
u = resolution(1)* I ./ ppd_h ; 
v = resolution(2)* J ./ ppd_v ; 
% u, v is in cycles /image
% meanningful range for u,v: 0 to rows/2 in vertical, 0 to columns/2 in hotizontal
 
D = ( I.^2 + J.^2 ).^0.5;  % distance to origin

%%%%%%%%% CSF

p = D;
P = 250;
theta = 0;
d = 1.5; % distance in meter

ra = 0.856 * d ^0.14;
rc = 1;
rtheta = 0.11*cos(4*theta)+0.89;

Al =0.801 * (1+0.7./La)^(-0.2);
Bl = 0.3*(1+100./La)^0.15;
S1_p = ((3.23 * p.^(-0.6)).^5 +1).^(-0.2) .* Al.*0.9.*p.*exp(- (Bl.*0.9.*p)).*(1 + 0.06*exp(Bl.*0.9.*p)).^0.5;
S1_pr = ((3.23 * (p/(ra*rc*rtheta)).^(-0.6)).^5 +1).^(-0.2) .* Al.*0.9.*p.*exp(- (Bl.*0.9.*(p/(ra*rc*rtheta)))).*(1 + 0.06*exp(Bl.*0.9.*(p/(ra*rc*rtheta)))).^0.5;

S = min(S1_pr,S1_p);
CSF_part = P*S;  % 


%%%% normalization

load('CVI.mat')
CVI=interp1(Luminance,ThresholdContrast,La);
diam = 4.9 - 3* tanh(0.4*(log10(La+1)));
OTF = exp(- (p./(20.9 - 2.1*diam)).^(1.3-0.07*diam));
nCSF = CVI * CSF_part./OTF;
nCSF = nCSF./max(max(nCSF));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


[XI, YI] = meshgrid (0:resolution(1)/2, 0:resolution(2)/2);
ZI = interp2 (u,v,nCSF,XI,YI,'linear',0);


CSF = zeros (resolution)';
CSF(resolution(2)/2 : resolution(2), resolution(1)/2 : resolution(1)) = ZI;

size_ZI = size(ZI);
ZI_crop = ZI (2:size_ZI(1)-1, 2:size_ZI(2)-1); 

CSF (resolution(2)/2 : resolution(2), 1 : (resolution(1)/2-1)) = fliplr(ZI(:,2:size_ZI(2)-1));
CSF (1:resolution(2)/2-1, resolution(1)/2 : resolution(1)) = flipud(ZI(2:size_ZI(1)-1,:));
CSF(1:resolution(2)/2-1, 1:resolution(1)/2-1 ) = flipud(fliplr(ZI_crop));
%%%%%%%%%%%%%%%%%%%%%%% fft and apply CSF

IM = fft2 (im);
IM_CSF = fftshift(IM).*CSF;
im_CSF = ifft2 (ifftshift(IM_CSF));
im_CSF = abs(im_CSF);
end

