function result= XYZ2LMS( im )

% http://en.wikipedia.org/wiki/LMS_color_space
% using Hunt-Pointer-Estevez (HPE) transformation matrix 
XYZtoLMS  = [ 0.3897,   0.6890,   -0.0787;...
			  -0.2298,    1.1834,    0.0464;...
			 0,   0,    1];
         
         for i = 1 : 3;

                result(:,:,i) =  XYZtoLMS(i,1)*im (:,:,1) + XYZtoLMS(i,2)*im (:,:,2) + XYZtoLMS(i,3)*im (:,:,3);

        end


end

