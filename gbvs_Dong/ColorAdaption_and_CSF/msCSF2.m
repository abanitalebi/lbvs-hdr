% multiscale CSF
% input im should be luminance of HDR image
% Luminance adaption values LA could be changed for images with different
% dynamic range
% this is a function take two input parameters for multiscale CSF
% luminance is from the original picture, i is the A channel from color
% adaption

function result = msCSF2( i, luminance )

%luminance = 20000* luminance./max(max(luminance));

LA = [0.0001, 0.01, 0.1, 1 , 10, 100, 1000]; % Luminance adaption

for k = 1: length(LA)
    im_CSF{k} = CSF(i, LA(k),10);
end

size_im = size(i);
im_CSF_combined = zeros(size_im );
% for m = 1 : size_im(1)
%     for n = 1 : size_im(2)
%         
%       if luminance (m,n) < LA(1)
%             im_CSF_combined (m,n) = im_CSF{1}(m,n);
%       elseif luminance(m,n) >= LA(1) & luminance(m,n) < LA(2)
%             im_CSF_combined (m,n) = interp1 ([LA(1),LA(2)], [im_CSF{1}(m,n),im_CSF{2}(m,n)], i(m,n));
%       elseif  luminance(m,n) >= LA(2) & luminance(m,n) < LA(3)
%             im_CSF_combined (m,n) = interp1 ([LA(2),LA(3)], [im_CSF{2}(m,n),im_CSF{3}(m,n)], i(m,n));
%       elseif luminance(m,n) >= LA(3) & luminance(m,n) < LA(4)
%             im_CSF_combined (m,n) = interp1 ([LA(3),LA(4)], [im_CSF{3}(m,n),im_CSF{4}(m,n)], i(m,n));
%       elseif  luminance(m,n) >= LA(4) & luminance(m,n) < LA(5)
%             im_CSF_combined (m,n) = interp1 ([LA(4),LA(5)], [im_CSF{4}(m,n),im_CSF{5}(m,n)], i(m,n));
%       elseif luminance(m,n) >= LA(5) & luminance(m,n) < LA(6)
%             im_CSF_combined (m,n) = interp1 ([LA(5),LA(6)], [im_CSF{5}(m,n),im_CSF{6}(m,n)], i(m,n));
%       elseif luminance(m,n) >= LA(6) & luminance(m,n) < LA(7)
%             im_CSF_combined (m,n) = interp1 ([LA(6),LA(7)], [im_CSF{6}(m,n),im_CSF{7}(m,n)], i(m,n));
%       else 
%             im_CSF_combined (m,n) = im_CSF{7}(m,n);
%       end
%                         
%         
%     end
% end
index = find(luminance < LA (1));
im_CSF_combined(index) = im_CSF{1}(index);

index = find (luminance >= LA(1) & luminance< LA(2));
im_CSF_combined(index) = im_CSF{2}(index) - (im_CSF{2}(index) - im_CSF{1}(index)).*(LA(2)-luminance(index))./(LA(2) - LA(1));

index = find( luminance >= LA(2) & luminance < LA(3));
im_CSF_combined(index) = im_CSF{3}(index) - (im_CSF{3}(index) - im_CSF{2}(index)).*(LA(3)-luminance(index))./(LA(3) - LA(2));

index = find( luminance >= LA(3) & luminance < LA(4));
im_CSF_combined(index) = im_CSF{4}(index) - (im_CSF{4}(index) - im_CSF{3}(index)).*(LA(4)-luminance(index))./(LA(4) - LA(3));

index = find( luminance >= LA(4) & luminance < LA(5));
im_CSF_combined(index) = im_CSF{5}(index) - (im_CSF{5}(index) - im_CSF{4}(index)).*(LA(5)-luminance(index))./(LA(5) - LA(4));

index = find( luminance >= LA(5) & luminance < LA(6));
im_CSF_combined(index) = im_CSF{6}(index) - (im_CSF{6}(index) - im_CSF{5}(index)).*(LA(6)-luminance(index))./(LA(6) - LA(5));

index = find( luminance >= LA(6) & luminance < LA(7));
im_CSF_combined(index) = im_CSF{7}(index) - (im_CSF{7}(index) - im_CSF{6}(index)).*(LA(7)-luminance(index))./(LA(7) - LA(6));

index = find( luminance >= LA(7) );
im_CSF_combined (index) = im_CSF{7}(index);


result = abs(im_CSF_combined);
 result = mat2gray(result);
 result = result.^0.3;
end

