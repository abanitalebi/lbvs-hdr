% LA   addaption luminance  LA = 0.0001, 0.01,1,100,1000
% RG and BY are the two opponent color channels in Red-Green and Bule-Yello
% X= 10; % angular object area , refer to Barten
% this function calculate 2D CSF function and apply the function to RG and
% BY respectively


function [ColorCSF_RG, ColorCSF_BY] = ColorCSF(RG,BY)

size_im = size(RG);
resolution = [size_im(2) size_im(1)];
[ppd_h, ppd_v] = pix_per_deg (40,resolution,150);  

%%%%%%%%%%%%%% 3D CSF
index = logspace( -2, 2, 256 );
index = [0, index];   % add 0 to the scale
[I,J] = meshgrid(index); % I and J is in cycles/degree
 
u = resolution(1)* I ./ ppd_h ; 
v = resolution(2)* J ./ ppd_v ; 

% u, v is in cycles /image
% meanningful range for u,v: 0 to rows/2 in vertical, 0 to columns/2 in hotizontal

D = ( I.^2 + J.^2 ).^0.5;  % distance to origin


%%%%%%%%%%%%%%%%%%%%%% color CSF
load('chromaCSF');

CSF_RG = a1_RG*exp((b1_RG*(D.^c1_RG))) + a2_RG*exp((b2_RG*(D.^c2_RG)));
CSF_BY = a1_BY*exp((b1_BY*(D.^c1_BY))) + a2_BY*exp((b2_BY*(D.^c2_BY)));
CSF_RG = CSF_RG./(max(max(CSF_RG))); % normalization
CSF_BY = CSF_BY./(max(max(CSF_BY)));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% interpolate CSF surface  (RG)
[XI, YI] = meshgrid (0:resolution(1)/2, 0:resolution(2)/2);
ZI_RG = interp2 (u,v,CSF_RG,XI,YI,'linear',0); % u,v here used since FFT coefficients are cycles/image

CSF_RG_2D = zeros (resolution)';
CSF_RG_2D(resolution(2)/2 : resolution(2), resolution(1)/2 : resolution(1)) = ZI_RG;

size_ZI= size(ZI_RG); % same for BY plane
ZI_crop_RG = ZI_RG (2:size_ZI(1)-1, 2:size_ZI(2)-1); 

CSF_RG_2D (resolution(2)/2 : resolution(2), 1 : (resolution(1)/2-1)) = fliplr(ZI_RG(:,2:size_ZI(2)-1));
CSF_RG_2D (1:resolution(2)/2-1, resolution(1)/2 : resolution(1)) = flipud(ZI_RG(2:size_ZI(1)-1,:));
CSF_RG_2D(1:resolution(2)/2-1, 1:resolution(1)/2-1 ) = flipud(fliplr(ZI_crop_RG));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% interpolate CSF surface  (BY)
ZI_BY = interp2 (u,v,CSF_BY,XI,YI,'linear',0);
CSF_BY_2D = zeros (resolution)';
CSF_BY_2D(resolution(2)/2 : resolution(2), resolution(1)/2 : resolution(1)) = ZI_BY;

ZI_crop_BY = ZI_RG (2:size_ZI(1)-1, 2:size_ZI(2)-1); 

CSF_BY_2D (resolution(2)/2 : resolution(2), 1 : (resolution(1)/2-1)) = fliplr(ZI_BY(:,2:size_ZI(2)-1));
CSF_BY_2D (1:resolution(2)/2-1, resolution(1)/2 : resolution(1)) = flipud(ZI_BY(2:size_ZI(1)-1,:));
CSF_BY_2D(1:resolution(2)/2-1, 1:resolution(1)/2-1 ) = flipud(fliplr(ZI_crop_BY));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RGfft = fft2 (RG);
ColorCSF_RG_fft = fftshift(RGfft).*CSF_RG_2D; % multipilication in frequency domain
ColorCSF_RG = ifft2 (ifftshift(ColorCSF_RG_fft)); % back to spatial/image domain
%%%%%%%%%%%%%%%%%%%%%%%%%%
BYfft = fft2 (BY);
ColorCSF_BY_fft = fftshift(BYfft).*CSF_BY_2D; % multipilication in frequency domain
ColorCSF_BY = ifft2 (ifftshift(ColorCSF_BY_fft)); % back to spatial/image domain

%%%%%%%%%%%%%%
ColorCSF_RG = abs(ColorCSF_RG); 
ColorCSF_BY = abs(ColorCSF_BY); 

 ColorCSF_RG = mat2gray(ColorCSF_RG);
 ColorCSF_BY = mat2gray(ColorCSF_BY);

end

