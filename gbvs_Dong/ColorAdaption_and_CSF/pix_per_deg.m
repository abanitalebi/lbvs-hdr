% pixel per degree calculation
function [ppd_h, ppd_v] = pix_per_deg( display_diagonal_in, resolution, viewing_distance )

% display_diagonal_in - diagonal display size in inches, e.g. 19, 14
% resolution - display resolution in pixels as a vector, e.g. [1024 768]
% viewing_distance - viewing distance in cm, e.g. 100cm

% display_diagonal_in = 40; %% HDR display 40 inch
% resolution = [1024 768];
% viewing_distance = 100; % 1 meter


display_height = display_diagonal_in * sind (atand ( resolution (2)/resolution (1)));  % in inch
display_width = display_diagonal_in * sind (atand ( resolution (1)/resolution (2)));   % in inch
% resolition (1) should be smaller tham resolution (2)
% angles are represneted in degress
% 1 inch = 2.54 cm

viewing_angle_v = atand ( 0.5 * display_height *2.54/viewing_distance) *2 ;
viewing_angle_h = atand ( 0.5 * display_width *2.54/viewing_distance) *2 ;

ppd_v= resolution(2) /viewing_angle_v;% pixel per degree, vertically
ppd_h= resolution(1) /viewing_angle_v;% pixel per degree, horizontally
ppd = ppd_v;
end