% to get coorelation coefficient from two matrix
% must be the same size
function cc = CorCoef( a, b )
a=double (a);
b=double (b);

cov=sum( sum((a - mean(a(:))).*(b - mean(b(:)))))/( size(a, 1)*size(a, 2) );
std_a = std(a(:));
std_b = std(b(:));
cc = cov/(std_a*std_b) ;
end

