function [rawfeatmaps, motionInfo] = getFeatureMaps_hdr( img , param , prevMotionInfo )

%
% this computes feature maps for each cannnel in featureChannels/
%

load mypath;

%%%%
%%%% STEP 1 : form image pyramid and prune levels if pyramid levels get too small.
%%%%

mymessage(param,'forming image pyramid\n');

levels = [ 2 : param.maxcomputelevel ];

is_color = (size(img,3) == 3);
img_Luminance =[];RG=[];BY=[];

if ( is_color ) % [imgr,imgg,imgb,imgi] = mygetrgb( img ); % r,g,b and gray
    [ RG_, BY_, A ] = HDRColorAppearance( img );
    RG=RG_;
    BY=BY_;
%     [RG, BY] = ColorCSF(RG_,BY_);
    img_Luminance = makeLuminanceMap(img);
else img_Luminance = img;
end


load('TVI_Daly')
 %img_luma = interp1(Luminance,Luma, 179* img_Luminance);
 img_luma = interp1(Luminance,Luma, img_Luminance);

  img_CSF = msCSF2(img_luma, img_Luminance);
%  img_CSF = msCSF2(A, img_Luminance);  % 
 % img_CSF = msCSF(img_Luminance);

imgLuma = {}; imgCSF = {}; imgRG = {}; imgBY ={}; imgLuminance = {};
imgLuma{1} = gaussianSubsample(img_luma);
imgCSF{1} = gaussianSubsample (img_CSF);
imgRG{1} = gaussianSubsample(RG); 
imgBY{1} = gaussianSubsample(BY);
imgLuminance {1} = gaussianSubsample (img_Luminance);
%imgR{1} = gaussianSubsample(imgr); imgG{1} = gaussianSubsample(imgg); imgB{1} = gaussianSubsample(imgb);

for i=levels

    imgLuma{i} = gaussianSubsample( imgLuma{i-1} );
    imgCSF{i} = gaussianSubsample (imgCSF{i-1});
    imgLuminance{i} = gaussianSubsample (imgLuminance{i -1});
    if ( is_color )
        imgRG{i} = gaussianSubsample(imgRG{i-1});
        imgBY{i} = gaussianSubsample(imgBY{i-1});        
%         imgR{i} = gaussianSubsample( imgR{i-1} );
%         imgG{i} = gaussianSubsample( imgG{i-1} );
%         imgB{i} = gaussianSubsample( imgB{i-1} );
    else
        imgRG{i} = []; imgBY{i}= []; 
    end
    if ( (size(imgLuma{i},1) < 3) | (size(imgLuma{i},2) < 3 ) )  % original was 3
        mymessage(param,'reached minimum size at level = %d. cutting off additional levels\n', i);
        levels = [ 2 : i ];
        param.maxcomputelevel = i;
        break;
    end

end

%%% update previous frame estimate based on new frame
if ( (param.flickerNewFrameWt == 1) || (isempty(prevMotionInfo) ) )
    motionInfo.imgL = imgLuma;
else    
    w = param.flickerNewFrameWt;    
    for i = levels,
        %%% new frame gets weight flickerNewFrameWt
        motionInfo.imgL =  w * imgLuma{i} + ( 1 - w ) * prevMotionInfo.imgLuma{i};
    end
end
    
%%%
%%% STEP 2 : compute feature maps
%%%

mymessage(param,'computing feature maps...\n');

rawfeatmaps = {};

%%% get channel functions in featureChannels/directory

channel_files = dir( [pathroot '/util/featureChannelsHDR/*.m'] );

motionInfo.imgShifts = {};

for ci = 1 : length(channel_files)
  
    %%% parse the channel letter and name from filename
    parts = regexp( channel_files(ci).name , '^(?<letter>\w)_(?<rest>.*?)\.m$' , 'names');
    if ( isempty(parts) ), continue; end % invalid channel file name
    
    channelLetter = parts.letter;
    channelName = parts.rest;
    channelfunc = str2func(sprintf('%s_%s',channelLetter,channelName));
    useChannel = sum(param.channels==channelLetter) > 0;

    if ( ((channelLetter == 'C') || (channelLetter=='D')) && useChannel && (~is_color) )
        mymessage(param,'oops! cannot compute color channel on black and white image. skipping this channel\n');
        continue;
    elseif (useChannel)

        mymessage(param,'computing feature maps of type "%s" ... \n', channelName);

        obj = {};
        obj.info = channelfunc(param);
        obj.description = channelName;

        obj.maps = {};
        obj.maps.val = {};

        %%% call the channelfunc() for each desired image resolution (level in pyramid)
        %%%  and for each type index for this channel.

        for ti = 1 : obj.info.numtypes            
            obj.maps.val{ti} = {};
            mymessage(param,'..pyramid levels: ');
            for lev = levels,                
                mymessage(param,'%d (%d x %d)', lev, size(imgLuma{lev},1), size(imgLuma{lev},2));                
                if ( (channelLetter == 'F') || (channelLetter == 'M') || (channelLetter == 'H')|| (channelLetter == 'V'))                   
                    if ( ~isempty(prevMotionInfo) )
                        prev_img = prevMotionInfo.imgL{lev};
                    else
                        prev_img = imgLuma{lev};
                    end
                    
                    if ( ~isempty(prevMotionInfo) && isfield(prevMotionInfo,'imgShifts') && (channelLetter == 'M') )
                      prev_img_shift = prevMotionInfo.imgShifts{ti}{lev};
                    else
                      prev_img_shift = 0;
                    end

                    map = channelfunc(param,imgLuma{lev},prev_img,prev_img_shift,ti);                    
                    if (isfield(map,'imgShift'))
                       motionInfo.imgShifts{ti}{lev} = map.imgShift; 
                    end                    
                else
                    map = channelfunc(param,imgLuma{lev},imgCSF{lev},imgBY{lev},imgRG{lev},ti);
                end    
                obj.maps.origval{ti}{lev} = map.map;
                map = imresize( map.map , param.salmapsize , 'bicubic' );
                obj.maps.val{ti}{lev} = map;
            end
            mymessage(param,'\n');
        end

        %%% save output to rawfeatmaps structure
        eval( sprintf('rawfeatmaps.%s = obj;', channelName) );

    end
end

