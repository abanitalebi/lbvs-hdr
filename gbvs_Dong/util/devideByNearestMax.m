function output = devideByNearestMax( salmap , MaxLocation)
% MaxLocation is a 1 X N cell
mapsize = size(salmap);
K = length(MaxLocation); % number of maximums
output = zeros(mapsize);
for i = 1 : mapsize(1)
    for j = 1 : mapsize(2)
        for k = 1 : K
            pix_dist(k) = dist([i ,j], MaxLocation{k}');
        end
        [min_dist,min_index] = min(pix_dist);
        output(i, j) = salmap(i,j)/ salmap(MaxLocation{min_index}(1), MaxLocation{min_index}(2));
    end
end
end

