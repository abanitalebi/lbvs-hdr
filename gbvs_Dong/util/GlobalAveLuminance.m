% this calculates the global 
% im shoud be hdr image or intensiry of hdr image

function Lga = GlobalAveLuminance( im )

delta = 0.000001;
numPixels = size(im,1) * size(im,2);

if (size(im,3)==3)
    luminanceMap = makeLuminanceMap (im);
else
    luminanceMap = im;  % input is intensity
Lga = exp((1/numPixels)*(sum(sum(log(luminanceMap + delta)))));

end

