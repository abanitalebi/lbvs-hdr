function out = I_intensityHDR( fparam, imgLuma ,imgCSF, imgBY, imgRG, typeidx )

if ( nargin == 1)  
  out.weight = fparam.intensityWeight;
  out.numtypes = 1;
  out.descriptions{1} = 'Intensity';    
else
  out.map = imgCSF;
end
