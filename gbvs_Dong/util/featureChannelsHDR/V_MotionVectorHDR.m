function out = V_MotionVectorHDR( param, img , img_prev, prev_img_shift , ti )

if ( nargin == 1 )
    out.weight = param.flickerWeight;
    out.numtypes = 1;
    out.descriptions{1} = 'MotionVector';
else
    if ( (size(img,1) < 20) | (size(img,2) < 20 ) ) 
        out.map = zeros(size(img));
    else
        
    opts.BlockSize   = 10;
    opts.SearchLimit = 10;
    [MVx, MVy] = Bidirectional_ME(img_prev, img, opts);
    motion = ( MVx.^2 + MVy.^2 ).^ (1/2);
    
    out.map = motion;
    end


end


