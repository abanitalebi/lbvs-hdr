function out = H_hierachicalMotionHDR( param, img , img_prev, prev_img_shift , ti )

if ( nargin == 1 )
    out.weight = param.hierachicalMotionWeight;
    out.numtypes = 1;
    out.descriptions{1} = 'HierachicalMotion';
else
    if ( (size(img,1) < 20) | (size(img,2) < 20 ) )
        out.map = zeros(size(img));
    else
        H = fspecial('gaussian', [5 ,5], 0.5);
        blurred = imfilter(img_prev,H,'replicate');
        img_prev_r = img_prev - blurred;
        
        blurred = imfilter(img,H,'replicate');
        img_r = img - blurred;

        opticalFlow = vision.OpticalFlow ;
        opticalFlow. ReferenceFrameSource = 'Input port';
        % opticalFlow. Method = 'Lucas-Kanade';
        v = step(opticalFlow,img_r,img_prev_r);
        v = medfilt2(v);
%        out.map = v;



        level = graythresh(v);
        if (level == 0)  %%% when img == img_prev, v is all zero
            out.map = v;
        else
            index = find(v > level);
            temp = interp1( [level, max(max(v))], [0.5, 1], v(index));
            v(index) = temp;
            index = find(v <= level);
            v(index) = 0;
            
            %         BW = im2bw(v,level);
            %         BW = medfilt2(BW);
            out.map = v;
        end
    end
   
    
end
