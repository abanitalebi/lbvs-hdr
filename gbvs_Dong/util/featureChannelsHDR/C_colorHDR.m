function out = C_colorHDR( fparam, imgLuma ,imgCSF, imgBY, imgRG, typeidx )

if ( nargin == 1 )

  out.weight = fparam.colorWeight;
  
  out.numtypes = 2;
  out.descriptions{1} = 'Blue-Yellow';
  out.descriptions{2} = 'Red-Green';

else
  if ( typeidx ) == 1
    out.map = imgBY;  % 'Blue-Yellow'
  else
    out.map = imgRG;    % 'Red-Green'
  end
end

