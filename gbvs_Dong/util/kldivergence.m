function d = kldivergence(a, b)


% % fix bad indices in case we have sparse data:
% badidx = find(b == 0 | a == 0); a(badidx) = []; b(badidx) = [];
% %if (length(badidx) > 0), disp('WARNING: some zeros in kldistance!'); end
DRrange = 10;
a = 10*mat2gray(a);
b = 10*mat2gray(b);
% transform histograms into prob densities:
delta = 0.0001;
aa = (a+delta) ./ sum(sum(a+delta)); 
bb = (b+delta) ./ sum(sum(b+delta));

% compute distance:


% d = 0.5*(sum(sum(aa .* log(aa ./ bb))) + sum(sum(bb .* log(bb ./ aa))));

d = sum(sum(aa .* log(aa ./ bb))) ;