clear all
close all


directory_name = uigetdir;
fileFolder=directory_name;
dirOutput = dir(fullfile(fileFolder,'*.hdr'));
fileNames = {dirOutput.name}';
N = numel(fileNames);

resultFolder=('D:\Personal _Files\Research\PhD_Works\Research_Docs\HDR_Saliency\LBVS_HDR_web_v2\temp_tibul\');

for i = 1 : N
    fname{i} = strcat(fileFolder,'\',fileNames{i});
end

params = makeGBVSParams; % get default GBVS params
params.useIttiKochInsteadOfGBVS = 1;
params.ittiDeltaLevels = [ 3 4 ];
params.channels = 'CIO';
params.verbose = 1;
params.unCenterBias = 0;
params.frameGap = 5;
params.fusionMethod = 'ave'; % 'CNSP' or 'ave' or 'Minkowski'or 'CNMC'

% %%%%%%%%% spatial
for i = 1 : N
    img = hdrread(fname{i});
    Spatial = HVSHDR_img(img);
    result.Spatial.master{i}=Spatial.master_map;
    result.Spatial.Color{i}= Spatial.top_level_feat_maps{1};
    result.Spatial.Intensity{i}= Spatial.top_level_feat_maps{2};
    result.Spatial.Orientation{i}= Spatial.top_level_feat_maps{3};
%     imwrite(result.Spatial.master{i}, strcat(resultFolder,'Spatial\',num2str(i),'.png'), 'png');
    imwrite(result.Spatial.Color{i}, strcat(resultFolder,'C\',num2str(i),'.png'), 'png');
    imwrite(result.Spatial.Intensity{i}, strcat(resultFolder,'I\',num2str(i),'.png'), 'png');
    imwrite(result.Spatial.Orientation{i}, strcat(resultFolder,'O\',num2str(i),'.png'), 'png');
end

%%%%%%%%%% motion map
load('TVI_Daly')
motion_buffer = {};     % size is equal to T
T = params.frameGap;

for i = 1 : T
    img = hdrread(fname{i});
    img_Luminance = makeLuminanceMap(img);
    img_luma = interp1(Luminance,Luma, img_Luminance);
    motion_buffer{i} = img_luma;
end

for i = T+1 : N
    img = hdrread(fname{i});
    img_Luminance = makeLuminanceMap(img);
    img_luma = interp1(Luminance,Luma, img_Luminance);
    Motion = MotionMap(img_luma, motion_buffer{1}, params.salmapmaxsize);
    result.Motion{i} = Motion;
    imwrite(result.Motion{i}, strcat(resultFolder,'M\',num2str(i),'.png'), 'png');
   
    for j = 1 : T-1
        motion_buffer{j} = motion_buffer{j+1};
    end
    motion_buffer{T} = img_luma;
end

save(strcat(resultFolder,'\','1','-',num2str(i),'.mat'),'result' ,'-v7.3');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

