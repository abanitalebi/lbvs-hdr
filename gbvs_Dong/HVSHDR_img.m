function out = HVSHDR_img( img )

params = makeGBVSParams; % get default GBVS params
params.useIttiKochInsteadOfGBVS = 1;
params.channels = 'CIO';
params.verbose = 1;
params.unCenterBias = 0;
% params.interChannelComp = 0;
% params.numOfWTA = 5;
% params.salmapmaxsize = round( max(size(img))/64 );
params.fusionMethod = 'ave'; 

out = gbvs_hdr( img, params );
end

