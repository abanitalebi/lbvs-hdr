# LBVS-HDR: 

## Learning-Based Visual Saliency model for High Dynamic Range Video

Disclaimer: This code is free for use only for research purposes. For any commrecial use please contact us at: amin [dot] banitalebi [at] gmail [dot] com

Please cite the following references (whichever more appropriate) if you need to cite this code:

1) A. Banitalebi-Dehkordi, Y. Dong, M. T. Pourazad, and Panos Nasiopoulos, "A Learning Based Visual Saliency Fusion Model For High Dynamic Range Video (LBVS-HDR)," 23rd European Signal Processing Conference, EUSIPCO 2015.

    arxiv: https://arxiv.org/abs/1803.04827

    camera ready: http://ece.ubc.ca/~dehkordi/Files/papers/2015_EUSIPCO_Dehkordi_LBVS_HDR.pdf

    published: http://ieeexplore.ieee.org/document/7362642/



2) A. Banitalebi-Dehkordi1, M. Azimi, M. T. Pourazad, and P. Nasiopoulos1, "Saliency-Aided HDR Quality Metrics," ISO/IEC JTC1/SC29/WG11 MPEG2014/m37317, Oct. 2015, Geneva, Switzerland.

    camera ready: http://ece.ubc.ca/~dehkordi/Files/papers/2015_MPEG_Dehkordi_m37317_v2_Saliency_Aided_HDR_Metrics.pdf
    
    published: http://phenix.int-evry.fr/jct/
    


## How to use the code (version 1.0):

1) Generate the HDR saliency features:
- This version of the software only supports HDR video sequences in the format of "*.hdr" files. Copy your "*.hdr" files in a sorted frame name order to an input directory.
- Navigate MATLAB to "gbvs_Dong" directory. Run the "gbvs_install.m" script first, and then open "Dong_HDR_features.m" script and set your desired output feature folder. Run this script and select the input HDR folder.
- This code will generate various saliency features.

2) Prepare the features for LBVS-HDR code:
- Run the "prepare_features_LBVS_HDR.m" script with the appropriate input/output paths. This will prepare the features in a format needed by the learning module. 

3) Generate the saliency maps using LBVS-HDR:
- Run the "map_fusion_RF.m" script with the appropriate input features path. Note that since you are only using the framework in a testing mode (i.e. using the already generated model - the 3GB file), you won't need to set the training parameters. However, we provide the training code as well, so you can create your own models tailored for your own datasets.

Please feel free to send your questions to: amin [dot] banitalebi [at] gmail [dot] com












