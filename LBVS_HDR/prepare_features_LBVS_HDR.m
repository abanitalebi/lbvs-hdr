
close all
clear all
clc

load('..\temp_tibul\1-241.mat');

[~, nFrames] = size(result.Motion);
[height, width] = size(result.Spatial.Color{1,1});
features = zeros(height, width, nFrames, 4);

result.Motion{1,1} = result.Motion{1,6};
result.Motion{1,2} = result.Motion{1,6};
result.Motion{1,3} = result.Motion{1,6};
result.Motion{1,4} = result.Motion{1,6};
result.Motion{1,5} = result.Motion{1,6};

for f = 1:nFrames
    
    M = (result.Motion{1,f});
    C = (result.Spatial.Color{1,f});
    I = (result.Spatial.Intensity{1,f});
    O = (result.Spatial.Orientation{1,f});
    
    features(:, :, f, 1) = M;
    features(:, :, f, 2) = C;
    features(:, :, f, 3) = I;
    features(:, :, f, 4) = O;
    
end

save('features_tibul.mat','features','-v7.3');
