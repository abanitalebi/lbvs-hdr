tic
% This is to fuse the conspicuty maps to generate a saliency map using Random Forests
clear all; close all; clc;
warning off all

nTrees = 60;             % Use 40
numFeatures = 4;         %  4
trainPercent = 10;       % percentage of the training data to be used = trainPercent / 1620

MinLeaf = 10;   FBoot = 1;  SampleWithReplacement = 'True';     SampleRatio = 1/3;     
% nTrees=255; nof_max_Train = 50; resize_factor = 4; 'more videos'; MinLeaf = 10; FBoot=1; SampleWithReplacement = 'True';   SampleRatio = 1/3;   

rng default
type = 'regression';
% type = 'classification';
% labels = {'Color', 'Intensity', 'Orientation', 'Motion'};

width = 480;
height = 270;
format = '420';

% Loading the data
% load('E:\PhD\Documents\HDR\LBVS-HDR\LBVShdr\Code\groundTruth\fixationMap_test.mat');
load('features_tibul.mat');     features_test = features;

% load('E:\PhD\Documents\HDR\LBVS-HDR\LBVShdr\Code\groundTruth\fixationMap_train.mat');
% load('E:\PhD\Documents\HDR\LBVS-HDR\LBVShdr\Code\features\features_train.mat');

% % Training a model
% [h, w, nF, nFeatures] = size(features_train);
% 
% NVarToSample = floor(h*w*nF*SampleRatio);
% for j = 1:trainPercent:nF
%     i = (j - 1) / trainPercent + 1;
%     features_train_new(:,:,i,:) = features_train(:,:,j,:);
%     fixationMap_train_new(:,:,i,:) = fixationMap_train(:,:,j,:);
% end 
% [RFmodel, FeatureImportance] = train_saliency_RF(features_train_new, fixationMap_train_new, nTrees, type, MinLeaf, FBoot, opt);
% FeatureImportance = FeatureImportance / max(FeatureImportance(:));
% [Importances, locations] = sort(FeatureImportance, 'descend');
% ImportantFeatures = labels(locations);
% 
% clear('features_train');
% clear('fixationMap_train');
% RFmodel_name = strcat( 'RFmodel', '_nTrees_', num2str(nTrees), '_trainPercent_', num2str(100/trainPercent), '.mat' );
% save(RFmodel_name, '-v7.3', 'RFmodel', 'FeatureImportance');
% clc
% disp('Training finished, now testing ...');        


% loading a RF model which is previously saved
RFmodel_name = strcat( 'RFmodel', '_nTrees_', num2str(nTrees), '_trainPercent_', num2str(100/trainPercent), '.mat' );
load(RFmodel_name);


% Testing the created model over the test videos
% numberOfFrames = [151, 439, 400, 222];
[h, w, nFtest, nFeatures] = size(features_test);
for i = 1:nFtest
    conspicuty_maps_test = features_test(:,:,i,:);
    test_features = reshape(conspicuty_maps_test, [h*w, numFeatures]);

    % Testing a model
    classes = test_saliency_RF(RFmodel, test_features, type);
    saliency_map_1 = reshape(classes, [h,w]);
    Saliency_map2 = saliency_map_1 / max(saliency_map_1(:));

%     % add blur & center bias to test the AUC: blur: 5  &  CenterBias: 70
%     CenterBias = 70;     blur_factor = 5;
%     hm = fspecial('gaussian', [height, width], CenterBias);      hm = hm / max(hm(:));      Saliency_map2 = double(Saliency_map2) / double(max(Saliency_map2(:)));
%     hhm = fspecial('gaussian', [height, width], blur_factor);      hhm = hhm / max(hhm(:));
%     Saliency_map3 = (Saliency_map2 + hm) / 2;
%     saliency_map_4 = imfilter(Saliency_map3, hhm);
%     saliency_map_4 = uint8(255*saliency_map_4 / max(saliency_map_4(:)));

    Saliency_map3 = imresize(Saliency_map2, [1080, 1920]);
    saliency_maps(:,:,i) = Saliency_map3;
    imwrite(Saliency_map3, strcat('saliency_map_', sprintf('%04d', i), '.png'));
    
    disp(strcat('Percent Completed:', num2str(i/nFtest*100) ));
end
   
clear('features_test');
SaliencyMap_name = strcat( 'SaliencyMap', '_nTrees_', num2str(nTrees), '_trainPercent_', num2str(100/trainPercent), '.mat' );
save(SaliencyMap_name, '-v7.3', 'saliency_maps');

toc
